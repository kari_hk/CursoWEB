angular
	.module('Curso', ['ui.router', 'ui.bootstrap', 'ngResource'])
	.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider){
		//configurar o state padrão
		$urlRouterProvider.otherwise("/")/
		
		$stateProvider
			.state('inicio', {
				url: "/",
			    templateUrl: 'resources/inicio/view-inicio.html'			    
	})
	.state('sobre', {
		url: "/sobre",
		templateUrl: 'resources/sobre/view-sobre.html'
	})
	.state('usuarios', {
		url: "/usuarios",
		templateUrl: 'resources/usuarios/view-usuarios.html',
		controller: 'UsuarioController'
	})
	.state('usuariosEdit', {
		url: "/usuarios/:id",
		templateUrl: 'resources/usuarios/view-usuarios-edit.html',
		controller: 'UsuariosEditController'
	});
}])