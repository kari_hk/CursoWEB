angular
	.module('Curso')
	.factory('UsuariosRepository', ['$resource', function($resource) {
		return $resource('/rest/usuarios/:id', {id : '@id'});
		
	}])
	;
