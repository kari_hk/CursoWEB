angular
	.module('Curso')
	.controller('UsuariosEditController', ['$scope', 'UsuariosService', '$stateParams', '$state', 
	                                       function($scope, UsuariosService, $stateParams, $state){
		
		
		$scope.edit = function() {
			if ($stateParams.id != 'novo') {
				UsuariosService.findOne($stateParams.id).then(function(usuario) {
					$scope.usuario = usuario;
				});
			} else {
				$scope.usuario = {};
			}
			
		};
		
		$scope.save = function() {
			UsuariosService.save($scope.usuario).then(function() {
				$state.go('usuarios');
				
			});
		};
	}])
;