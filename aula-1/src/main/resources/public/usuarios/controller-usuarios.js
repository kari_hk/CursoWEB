angular
	.module('Curso')
	.controller('UsuarioController', ['$scope', 'UsuariosService', '$uibModal', function($scope, UsuariosService, $uibModal) {
		
		$scope.carregar = function() {
			UsuariosService
				.findAll()
				.then(function(usuarios) {
					$scope.usuarios = usuarios;
				})
			;
		};
		
		$scope.remove = function(usuario) {
			$uibModal
				.open({
					template: `
				    <div class="modal-body">
				        <p>Deseja realmente excluir ${usuario.nome}?</p>
				    </div>
				    <div class="modal-footer">
						<button type="button" class="btn btn-default" ng-click="fechar(false)">Não</button>
						<button type="button" class="btn btn-primary" ng-click="fechar(true)">Sim</button>
				    </div>
					`,
					controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
						$scope.fechar = function(excluir) {
							$uibModalInstance.close(excluir);
						};
					}]
				})
				.result.then(function(excluir) {
					if (excluir) {
						UsuariosService.remove(usuario.id).then(function() {
							$scope.carregar();
						});
					}
				});
		};
	
	}])
	;