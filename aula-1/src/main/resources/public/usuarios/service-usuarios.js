angular
	.module('Curso')
	.service('UsuariosService', ['UsuariosRepository', function(UsuariosRepository) {
		this.findAll = function() {
			return UsuariosRepository.query().$promise;						
		};		
		
		this.findOne = function(id) {
			return UsuariosRepository.get({id:id}).$promise;
		};
		
		this.save = function(usuario) {
			return UsuariosRepository.save(usuario).$promise;
		};
		
		this.remove = function(id) {
			return UsuariosRepository.remove({id:id}).$promise;
		}
	}])
	;