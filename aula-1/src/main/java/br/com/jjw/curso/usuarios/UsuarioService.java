package br.com.jjw.curso.usuarios;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	public List<UsuarioDto> findAll() {
		return usuarioRepository.findAll().stream().map(usuario -> {
			return convert(usuario);
		}).collect(Collectors.toList());
	}

	private UsuarioDto convert(Usuario usuario) {
		UsuarioDto domain = new UsuarioDto();

		domain.setId(usuario.getId());
		domain.setApelido(usuario.getApelido());
		domain.setNome(usuario.getNome());
		domain.setIdade(usuario.getIdade());
		return domain;
	}

	public UsuarioDto findOne(Integer id) {
		return convert(usuarioRepository.findOne(id));
	}

	public UsuarioDto save(UsuarioDto usuario) {
		Usuario usu;
		if (usuario.getId() != null) {
			usu = usuarioRepository.findOne(usuario.getId());
		} else {
			usu = new Usuario();
		}

		usu.setApelido(usuario.getApelido());
		usu.setNome(usuario.getNome());
		usu.setIdade(usuario.getIdade());

		usu = usuarioRepository.save(usu);
		return convert(usu);
	}

	public void remove(Integer id) {
		usuarioRepository.delete(id);
	}
}
