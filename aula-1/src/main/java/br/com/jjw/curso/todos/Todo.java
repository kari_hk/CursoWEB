package br.com.jjw.curso.todos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.jjw.curso.usuarios.Usuario;

@Entity
@Table(name = "todo")
@SequenceGenerator(name = "seq_todo", initialValue = 1, sequenceName = "seq_todo")
public class Todo implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_todo")
	private Integer id;

	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "concluido")
	private Boolean concluido;
	
	@ManyToOne
	@JoinColumn(name = "USUARIO_ID")
	@Column(name = "usuario")
	private Usuario usuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getConcluido() {
		return concluido;
	}

	public void setConcluido(Boolean concluido) {
		this.concluido = concluido;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
