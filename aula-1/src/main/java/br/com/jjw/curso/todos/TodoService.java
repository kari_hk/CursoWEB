package br.com.jjw.curso.todos;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TodoService {


	@Autowired
	private TodoRepository todoRepository;

	public List<TodoDto> findAll() {
		return todoRepository.findAll().stream().map(todo -> {
			return convert(todo);
		}).collect(Collectors.toList());
	}

	private TodoDto convert(Todo todo) {
		TodoDto domain = new TodoDto();

		domain.setId(todo.getId());
		domain.setDescricao(todo.getDescricao());
		domain.setConcluido(todo.getConcluido());
		domain.setUsuario(todo.getUsuario());
		return domain;
	}

	public TodoDto findOne(Integer id) {
		return convert(todoRepository.findOne(id));
	}

	public TodoDto save(TodoDto todo) {
		Todo td;
		if (todo.getId() != null) {
			td = todoRepository.findOne(todo.getId());
		} else {
			td = new Todo();
		}

		td.setDescricao(todo.getDescricao());
		td.setConcluido(todo.getConcluido());
		td.setUsuario(todo.getUsuario());

		td = todoRepository.save(td);
		return convert(td);
	}

	public void remove(Integer id) {
		todoRepository.delete(id);
	}

}
