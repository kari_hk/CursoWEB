package br.com.jjw.curso.todos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/todos")
public class TodoRestController {

	@Autowired
	private TodoService todoService;

	@GetMapping
	public List<TodoDto> findAll() {
		return todoService.findAll();

	}

	@GetMapping("/{id}")
	public TodoDto findOne(@PathVariable Integer id) {
		return todoService.findOne(id);
	}

	@PostMapping
	public TodoDto save(@RequestBody TodoDto todo) {
		return todoService.save(todo);
	}

	@PostMapping("/{id}")
	public TodoDto save(@PathVariable Integer id, @RequestBody TodoDto todo) {
		return todoService.save(todo);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Integer id) {
		todoService.remove(id);
	}
}
