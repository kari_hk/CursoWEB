package br.com.jjw.curso.usuarios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/usuarios")
public class UsuarioRestController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping
	public List<UsuarioDto> findAll() {
		return usuarioService.findAll();

	}

	@GetMapping("/{id}")
	public UsuarioDto findOne(@PathVariable Integer id) {
		return usuarioService.findOne(id);
	}

	@PostMapping
	public UsuarioDto save(@RequestBody UsuarioDto usuario) {
		return usuarioService.save(usuario);
	}

	@PostMapping("/{id}")
	public UsuarioDto save(@PathVariable Integer id, @RequestBody UsuarioDto usuario) {
		return usuarioService.save(usuario);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Integer id) {
		usuarioService.remove(id);
	}

}
