package br.com.jjw.curso.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		super.addResourceHandlers(registry);
		registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/public/");
		registry.addResourceHandler("/resources/modules/**").addResourceLocations("classpath:/modules/");
	}

}
